import pandas as pd
import glob

all_df = []
all_data = pd.DataFrame()
for f in glob.glob("./Data/*.xlsx"):
    df = pd.read_excel(f, engine='openpyxl', index_col=0)

    #print(df.index.tolist())
    all_df.append(df)

all_data = pd.concat(all_df, axis=1)
#print(all_data)
all_data.to_excel("merged.xlsx")
